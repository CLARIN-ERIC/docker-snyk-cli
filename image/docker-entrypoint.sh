#!/bin/bash

# Wrapper that invokes the snyk cli to test a docker image for known vulnerabilities.
# Exits with a non-zero status code if there are know vulnerabilities at or above the
# specified severity treshold (defaults to high)
#
# Usage:
#   <script> test "${IMAGE_QUALIFIED_NAME}" --file="${PATH TO DOCKER FILE}" ${POLICY}
#
# Additional configuration via environment variables:
#   SCAN_SEVERITY_THRESHOLD       Translates into: --severity-threshold=<low|medium|high|critical>
#                                 Required, defaults to high if no value is specified
#                                 Report only vulnerabilities at the specified level or higher.
#   SCAN_FAIL_ON                  Translates into:--fail-on=<all|upgradable|patchable>
#                                 Optional, only used when a value is specified
#                                 Fail only when there are vulnerabilities that can be fixed.
#
#                                 -  all: fail when there is at least one vulnerability that can be either upgraded or patched.
#                                 -  upgradable: fail when there is at least one vulnerability that can be upgraded.
#                                 -  patchable: fail when there is at least one vulnerability that can be patched.
#
#                                  If vulnerabilities do not have a fix and this option is being used, tests pass.
#


SNYK_COMMAND="$1"
SNYK_PARAMS="${*:2}"
ADDITIONAL_ENV=""

#Add --severity-treshold flag
if [ "${SCAN_SEVERITY_THRESHOLD}" != "" ]; then
  printf 'Using severity threshold: %s\n' "${SCAN_SEVERITY_THRESHOLD}"
  SNYK_PARAMS="${SNYK_PARAMS} --severity-threshold=${SCAN_SEVERITY_THRESHOLD}"
else
  printf "Using default severity threshold: high\n"
  SNYK_PARAMS="${SNYK_PARAMS} --severity-threshold=high"
fi
#Add --fail-on flag if set
if [ "${SCAN_FAIL_ON}" != "" ]; then
  printf 'Using fail-on: %s\n' "${SCAN_FAIL_ON}"
  SNYK_PARAMS="${SNYK_PARAMS} --fail-on=${SCAN_FAIL_ON}"
fi

echo "command: ${SNYK_COMMAND}"
echo "params: ${SNYK_PARAMS}"

if [ -z "${USER_ID}" ]; then
  USER_ID=$(id -u)
fi

USER_NAME=$(getent passwd "${USER_ID}" | awk -F ':' '{print $1}')

if [ "${USER_NAME}" != "" ] && [ "${USER_NAME}" != "root" ]; then
  usermod -d /home/node "${USER_NAME}"
fi

useradd -o -m -u "${USER_ID}" -d /home/node docker-user 2>/dev/null

runCmdAsDockerUser() {
#  echo "command=[$1]"
  su docker-user -m -c "$1"

  return $?
}

exitWithMsg() {
  echo "Failed to run the process ..."

  if [ -f "$1" ]; then
    cat "$1"
  else
    echo "$1"
  fi

  exit "$2"
}

##
## Start of backward compatability code.
## Should be phased out when we phase out the current version of the jenkins
## plugin.
## These parameters should only be used with the Jenkins plugin! Please see
## README.md for more info.
##

TEST_SETTINGS=""
PROJECT_SUBDIR=""

if [ -n "${TARGET_FILE}" ]; then
  if [ ! -f "${PROJECT_PATH}/${PROJECT_FOLDER}/${TARGET_FILE}" ]; then
    exitWithMsg "\"${PROJECT_PATH}/${PROJECT_FOLDER}/${TARGET_FILE}\" does not exist" 1
  fi

  PROJECT_SUBDIR=$(dirname "${TARGET_FILE}")
  MANIFEST_NAME=$(basename "${TARGET_FILE}")
  TEST_SETTINGS="--file=${MANIFEST_NAME} "
fi

if [ -n "${ORGANIZATION}" ]; then
  TEST_SETTINGS="${TEST_SETTINGS} --org=${ORGANIZATION}"
fi

SNYK_PARAMS="${SNYK_PARAMS} ${TEST_SETTINGS}"

##
## End of backward compatability code
##

if [ -z "${SNYK_TOKEN}" ]; then
  exitWithMsg "Missing \${SNYK_TOKEN}" 1
fi

if [ -n "${ENV_FLAGS}" ]; then
  ADDITIONAL_ENV="-- ${ENV_FLAGS}"
fi

cd "${PROJECT_PATH}/${PROJECT_FOLDER}/${PROJECT_SUBDIR}" ||
  exitWithMsg "Can't cd to ${PROJECT_PATH}/${PROJECT_FOLDER}/${PROJECT_SUBDIR}" 1

runCmdAsDockerUser "PATH=${PATH} snyk ${SNYK_COMMAND} ${SNYK_PARAMS} ${ADDITIONAL_ENV}"

RC=$?

exit "$RC"